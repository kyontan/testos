#include "bootpack.h"
#include <string.h>

static SuperFS* ml_hnd_fsmdl;
Block* ml_free_blk_head;
Block* ml_all_blk;
static FileItem ml_filetbl[TFL_MAX_FILETBL];
inode* ml_free_inode_head;
inode* ml_all_inode;

struct SHEET *sht_back;
char s[40];

/**
 * @brief "create" procedure for block device
 *
 * @param filename (absolute path)
 * @param user_fdtbl
 * @return 0: success, error if others
 */
int te2fo_create(char* filename, FileItem** user_fdtbl) {

	FileItem* file;
	unsigned int new_inono, parent_inono;
	struct one_inode* p_inode;
	int i, fd;

	unsigned int** pp_dirty_inoblk;
	unsigned int* pi_buff;

	pp_dirty_inoblk = &pi_buff;
	p_inode = te2fo_create_new_dirent(&new_inono, &parent_inono,
			pp_dirty_inoblk,	filename, 0);
	if (!p_inode) {
		// debug_puts("couldn't create dirent");
		return -1;
	}

	// p_inode->i_mode = 0;
	p_inode->i_size = 0;
	p_inode->i_blocks = 0;
	p_inode->i_links_count = 1;

	for(i=0; i<blocks_num; i++)
		p_inode->i_block[i] = 0;

	file = tfl_alloc_filetbl();

	file->one_inode = p_inode;
	// file->offset = 0;

	inode i_node;
	file->inode = &i_node;	// TODO i-node割り当てしっかり
	file->inode->inono = new_inono; //p_entry->inode;
	file->inode->dev_no = 1;
	file->inode->i_mode = p_inode->i_mode;
	// file->ref_count = 1;
	file->p_dirty_inoblk = *pp_dirty_inoblk;
	*file->p_dirty_inoblk = 1;

	fd = tfl_alloc_file_usertbl(user_fdtbl, file);
	if (fd<0)
		return -2;

	return fd;
}


/**
 * @brief create new directory-entry
 * @param p_new_inono allocated new inode no (OUT parameter)
 * @param p_parent_inono parent inode no (OUT parameter)
 * @param p_dirty_inoblk dirty flag address of inode block (OUT parameter)
 * @param entry_abs_path entry absolute path to create
 * @param file_type new entry filetype
 * @return new entry inode, 0 if error
 *
 * create new directory-entry and return it's i_node
 */
struct one_inode* te2fo_create_new_dirent(unsigned int* p_new_inono,
		unsigned int* p_parent_inono,
		unsigned int** pp_dirty_inoblk,
		char* entry_abs_path, unsigned char file_type) {

	char dirpath[TFL_MAX_PATHSTR], filename[TFL_MAX_PATHSTR];

	struct one_inode* parent_inode;

	int idx_block, dir_inono;
	Block* blkcell;
	struct ext2_dir_entry* p_dirent;

	// 親ディレクトリの絶対パスとディレクトリ名を取得
	tfl_dirpath_file(dirpath, filename, entry_abs_path);

	// 親ディレクトリのi-nodeを取得
	if(!(parent_inode = te2fm_namei(&dir_inono, ml_hnd_fsmdl, dirpath))) {
		return 0;
	}

	idx_block = 0;
	// 親ディレクトリの1番目のブロックを取得
	blkcell = tflm_getblk_by_inode_index(parent_inode, idx_block++);

	// 新しいディレクトリのi-node番号を確保
	*p_new_inono = te2fm_alc_new_inono(ml_hnd_fsmdl);

	//-- LOOP Until all block exausted
	while(blkcell) {
		// existence check
		p_dirent = te2fs_lookup_dirent(filename, blkcell->blk,
				ml_hnd_fsmdl->hnd_fs.sz_block);

		// if entry alreay exists, free i-node no and error return
		if (p_dirent) {
			te2fm_free_inono(ml_hnd_fsmdl, *p_new_inono);
			// debug_puts("te2fo_create_new_dirent - entry already exists.");
			return 0;
		}

		// make one entry on the parent directory block
		p_dirent = te2fs_alloc_dirent(&ml_hnd_fsmdl->hnd_fs, blkcell->blk,
				*p_new_inono, file_type, filename);

		// if block entry is not available, read next block
		if (p_dirent) {
			blkcell->dirty = 1;
			break;
		}
		blkcell = tflm_getblk_by_inode_index(parent_inode, idx_block++);
	}
	//-- LOOP_END

	// if all block entry is not available, error return
	if (!p_dirent) {
		te2fm_free_inono(ml_hnd_fsmdl, *p_new_inono);
		return 0;
	}

	// get new directory i_node
	return tflm_get_inode_by_no(pp_dirty_inoblk, p_dirent->inode);
}


/**
 * @brief get directory path and file string from absolete path
 * @param dirpath directory path (OUT parameter)
 * @param file file name (OUT parameter)
 * @param abs_path absolete path
 */
void tfl_dirpath_file(char* dirpath, char* file, char* abs_path) {
	char* p_last_slash;

	strcpy(dirpath, abs_path);

	p_last_slash = strrchr(dirpath, '/');

	strcpy(file, p_last_slash+1);

	if (p_last_slash == dirpath)
		*++p_last_slash = 0;
	else
		*p_last_slash = 0;
}


/**
 * @brief get i_node from filepath
 * @param inono inode no (out parameter)
 * @param hnd_fsmdl
 * @param s_path filepath
 * @return i_node
 */
struct one_inode* te2fm_namei(int* inono, SuperFS* hnd_fsmdl,
		char* s_path) {
	char* s_cur_path;
	char s_path_buf[TFL_MAX_PATHSTR+1];
	Block* blkcell;
	struct ext2_dir_entry* p_dirent;
	struct one_inode* p_inode;
	int idx_slash;
	char name[16];
	unsigned int dir_blkno;

	strcpy(s_path_buf, s_path);

	s_cur_path = s_path_buf;

	if (!strcmp(s_cur_path, "/")) {	// ルートディレクトリであったら
		*inono = 2;
		return hnd_fsmdl->root_inode;
	}

	s_cur_path++;	// 最初のスラッシュは除く

	//TODO search all block. only block[0] searching at the moment.
	dir_blkno = hnd_fsmdl->root_inode->i_block[0];

	idx_slash = stridx(s_cur_path, '/');

	while(idx_slash >= 0) {
		strncpy(name, s_cur_path, idx_slash);
		name[idx_slash] = 0;
		s_cur_path += idx_slash + 1;

		blkcell = tblbm_get_blkcell(dir_blkno);

		p_dirent = te2fs_lookup_dirent(name, blkcell->blk, hnd_fsmdl->hnd_fs.sz_block);
		if (!p_dirent)
			return 0;

		p_inode = tflm_get_inode_by_dirent(p_dirent);

		dir_blkno = p_inode->i_block[0];

		// stridx is not standard library.
		// stridx must be implemented in mystring.h.
		idx_slash = stridx(s_cur_path, '/');
	}

	blkcell = tblbm_get_blkcell(dir_blkno);

	p_dirent = te2fs_lookup_dirent(s_cur_path, blkcell->blk, hnd_fsmdl->hnd_fs.sz_block);	// Something Wrong Here!
	return 0;
	// if (!p_dirent)
	// 	return 0;
	// *inono = p_dirent->inode;
	//
	// p_inode = tflm_get_inode_by_dirent(p_dirent);
	//
	// return p_inode;
}

/**
 * @brief get block by block-no
 * @param blkno   block-no (not lba)
 * @return  !=0: success, ==0: failed.
 */
Block* tblbm_get_blkcell(unsigned long blkno) {
	int rc;
	Block* blkcell;

	////////////////////////////////////////
	// lock task
	////////////////////////////////////////

	//lookup block-buffer
	blkcell = tblbf_lookup_blkcell(blkno);

	//return block if it's found.
	if (blkcell) {
		// TODO freeなブロックに対する操作
		////////////////////////////////////////
		// release task
		////////////////////////////////////////
		return blkcell;
	}

	// TODO MUST : 新しいブロックの割り当て``

	if (!blkcell) {
		return 0;
	}

	if (blkcell->dirty) {
		ide_ata_write_multiple_sector_pio(0, blkcell->blkno * 2, 2, blkcell->blk);
		blkcell->dirty = 0;
	}

	blkcell->blkno = blkno;
	////////////////////////////////////////
	// release task
	////////////////////////////////////////

	////////////////////////////////////////
	// lock task
	////////////////////////////////////////
	ide_ata_read_multiple_sector_pio(0, blkcell->blkno * 2, 2, blkcell->blk);
	////////////////////////////////////////
	// release task
	////////////////////////////////////////

	if (rc) return 0;

	return blkcell;

}


/**
 * @brief lookup blockcell by block-no
 * @param blkno block-no
 * @return blockcell. 0 if not found.
 */
Block* tblbf_lookup_blkcell(unsigned int blkno) {
	Block* blkcell;

	blkcell = ml_all_blk;
	while (blkcell->next_block) {
		blkcell = blkcell->next_block;
		if (blkcell->blkno == blkno) {
			return blkcell;
		}
	}
	return 0;
}

// TODO
// ブロックを削除する

// /**
//  * @brief remove blockcell from free-list
//  * @param blkcell blockcell to remove
//  * @return 0: success
//  */
// int tblbf_remove_from_free(Block* blkcell) {
// 	io_cli();
//
//
// 	blkcell->prev_free->next_free = blkcell->next_free;
// 	blkcell->next_free->prev_free = blkcell->prev_free;
//
// 	blkcell->next_free = blkcell->prev_free = 0;
// 	blkcell->free = 0;
//
// 	io_sti();
//
// 	return 0;
// }


// SOMETHING WRONG HERE!!!
/**
 * @brief lookup directory-entry
 * @param filename entry name looking up
 * @param dirent_blk directory-entry block
 * @param sz_block block size
 * @return directory-entry. 0 if filename coundn't be found.
 */
struct ext2_dir_entry*
te2fs_lookup_dirent(char* filename, unsigned char* dirent_block,
			  unsigned int sz_block) {

	struct ext2_dir_entry* p_entry;
	int file_name_len;
	int i = 570;

	p_entry = (struct ext2_dir_entry*)dirent_block;
	file_name_len = strlen(filename);
	while(p_entry) {
		sprintf(s, "NAME_LEN:%d, FILE_NAME_LEN:%d", (int)p_entry->name_len, file_name_len);
		putfonts8_asc_sht(sht_back, 0, i+20, COL8_FFFFFF, COL8_008484, s, 200);
		// not go to if statement... why???
		if (file_name_len == p_entry->name_len) {
			if (!strncmp(p_entry->name, filename, p_entry->name_len))
				return p_entry;
		}
		p_entry = te2fs_next_dirent(p_entry, dirent_block, sz_block);
		i += 20;
	}
	return 0;
}


// SOMETHING WRONG HERE!!!
/**
 * @brief get next directory-entry
 * @param p_entry		current directory-entry
 * @param dirent_block	directory-entry block
 * @param sz_block		block size
 * @return		!=0: next directory-entry, =0: the end of entry
 */
struct ext2_dir_entry*
te2fs_next_dirent(struct ext2_dir_entry* p_entry,
				  unsigned char* dirent_block, unsigned int sz_block) {

	unsigned char* p;

	p = (unsigned char*)p_entry;
	p += p_entry->rec_len;
	if (p >= dirent_block + sz_block)
		return 0;

	return (struct ext2_dir_entry*)p;

}


/**
 * @brief get inode by directory-entry
 * @param p_entry directory-entry
 * @return inode
 */
struct one_inode* tflm_get_inode_by_dirent(struct ext2_dir_entry* p_entry) {

	unsigned int inode_blkno;
	unsigned int inode_blkoff;
	Block* blkcell;

	if (!p_entry)
		return 0;	//file not found.

	inode_blkno = (p_entry->inode - 1) / 8 + ml_hnd_fsmdl->hnd_fs.start_inode_blkno;

	inode_blkoff = ((p_entry->inode - 1) % 8) * 128;

	blkcell = tblbm_get_blkcell(inode_blkno);

	if (!blkcell) {
		return 0;
	}

	return (struct one_inode*) (blkcell->blk + inode_blkoff);

}


/**
 * @brief get blockcell by inode index
 *
 * @param p_inode inode
 * @param idx_block  index of inode.i_block[]
 * @return allocated block, 0: block not allocated
 */
Block* tflm_getblk_by_inode_index(struct one_inode* p_inode,
		unsigned int idx_block) {

	Block* blkcell;
	Block* indirect_blkcell;
	unsigned int* p_indirect_block;
	unsigned int blkno, indirect_blkno;
	int i;
	char buff[128];

	indirect_blkcell = 0;

	if (idx_block < 12) {
		if (p_inode->i_block[idx_block] == 0) {	// 未割当のブロックだったら

			blkno = te2fm_alc_new_blkno(ml_hnd_fsmdl);

			p_inode->i_block[idx_block] = blkno;
			p_inode->i_blocks += 2;

		} else {
			blkno = p_inode->i_block[idx_block];
		}
		blkcell = tblbm_get_blkcell(blkno);
	} else {
		return 0;
	}

	return blkcell;

}


/**
 * @brief allocate new block-no on block-bitmap
 *
 * allocate new block-no on block-bitmap and
 * mark dirty on inode bitmap block.
 * @param hnd_fsmdl
 * @return allocated new block-no
 */
unsigned int te2fm_alc_new_blkno(SuperFS* hnd_fsmdl) {
	unsigned int blkno;

	blkno = te2fs_notalc_bmp(hnd_fsmdl->hnd_fs.blkbmp, hnd_fsmdl->hnd_fs.sz_block);
	te2fs_setbit_bmp(hnd_fsmdl->hnd_fs.blkbmp, blkno, 1);

	hnd_fsmdl->blkbmp_blkcell->dirty = 1;
	return blkno;
}


/**
 * @brief set bit on the bitmap.
 * @param bitmap   bitmap
 * @param no       no (start with 0)
 * @param bit      1: ON, 0: OFF
 */
void te2fs_setbit_bmp(unsigned char *bitmap, unsigned int no, int bit) {
	unsigned char* p;
	int idx;
	int lsht;
	unsigned char mask;

	// idx = te2fs_bindex_bmp(no);
  idx = (no - 1) / 8;

	// lsht = te2fs_blshft_bmp(no);
  lsht = (no - 1) % 8;

	p = bitmap + idx;


	if(bit) {
		mask = 1 << lsht;
		*p |= mask;
	} else {
		mask = 1 << lsht;
		mask = ~mask;
		*p &= mask;
	}
}


/**
 * @brief get not-allocated bitmap bit-index.
 * @param bitmap	bitmap
 * @param sz_block block-size
 * @return no (block-no or inode-no)
 */
unsigned int te2fs_notalc_bmp(unsigned char *bitmap, int sz_block) {
	int i, j;
	unsigned char c, mask;
	unsigned int no;

	no = 0;
	for (i=0; i<sz_block; i++) {
		c = bitmap[i];
		if(0xff != c) {
			no = i*8;
			mask = 1;
			for (j=0; j<8; j++) {
				if(!(c & mask)) {
					no += j;
					break;
				}
				mask <<= 1;
			}
			break;
		}
	}
	/* we assume bitmap not to be represented no '0' */
	return no + 1;

}


unsigned int te2fm_alc_new_inono(SuperFS* hnd_fsmdl) {
	unsigned int inono;

	inono = te2fs_notalc_bmp(hnd_fsmdl->hnd_fs.inobmp, hnd_fsmdl->hnd_fs.sz_block);
	te2fs_setbit_bmp(hnd_fsmdl->hnd_fs.inobmp, inono, 1);
	hnd_fsmdl->inobmp_blkcell->dirty = 1;

	return inono;
}

/**
 * @brief free inode-no
 *
 * free inode-no and mark dirty on inode bitmap block.
 * @param hnd_fsmdl
 * @param inono inode-no
 */
void te2fm_free_inono(SuperFS* hnd_fsmdl, unsigned int inono) {
	te2fs_free_inono(&hnd_fsmdl->hnd_fs, inono);
	hnd_fsmdl->inobmp_blkcell->dirty = 1;
}


/*
 * free inode-no
 */
void te2fs_free_inono(FileHandler* hnd_fs, unsigned int inono) {
	te2fs_setbit_bmp(hnd_fs->inobmp, inono, 0);
}


/**
 * @brief alloc new directory-entry
 * @param hnd_fs
 * @param dirent_blk directory-entry block
 * @param inono      new entry inode-no
 * @param file_type  new entry filetype
 * @param filename   new entry filename
 * @return allocated new directory-entry, 0 if no availabel entry
 */
struct ext2_dir_entry*
te2fs_alloc_dirent(FileHandler* hnd_fs, unsigned char* dirent_blk,
				   unsigned int inono,
				   unsigned char file_type,
				   char* filename) {

	struct ext2_dir_entry* p_entry;
	struct ext2_dir_entry* p_new_entry;
	unsigned int sz_new_fn;
	int sz_old_rec_len;
	int sz_min_cur_ent;
	int sz_new_ent;

	p_entry = (struct ext2_dir_entry*)dirent_blk;

	sz_new_fn = strlen(filename);
	sz_new_ent = te2fs_minsize_dirent(sz_new_fn);
	while (p_entry) {

		sz_min_cur_ent = te2fs_minsize_dirent(p_entry->name_len);

		if (p_entry->rec_len >= (sz_min_cur_ent + sz_new_ent)) {
			sz_old_rec_len = p_entry->rec_len;
			p_entry->rec_len = sz_min_cur_ent;
			p_new_entry = te2fs_next_dirent(p_entry, dirent_blk,
											hnd_fs->sz_block);
			p_new_entry->inode = inono;
			p_new_entry->rec_len = sz_old_rec_len - sz_min_cur_ent;
			p_new_entry->name_len = sz_new_fn;
			strcpy(p_new_entry->name, filename);
			return p_new_entry;
		}

		p_entry = te2fs_next_dirent(p_entry, dirent_blk, hnd_fs->sz_block);


	}
	return 0;
}


/*
 * get minimum directory-entry size
 * (must be a multiple of 4)
 */
int te2fs_minsize_dirent(int sz_filename) {

	int min, min4;

	// 4: inode size
	// 2: rec_len size
	// 2: name_len + file_type
	// 1: string terminator ('\0')
	min = sz_filename + 9;

	min4 = (min + 3) & 0xfffffffc;

	return min4;

}


/**
 * @brief get inode by inode no
 * @param pp_dirty_inoblk dirty flag double pointer of inode block (OUT parameter)
 * @param inono inode no
 * @return one_inode
 */
struct one_inode* tflm_get_inode_by_no(unsigned int** pp_dirty_inoblk,
		unsigned int inono) {

	unsigned int inode_blkno;
	unsigned int inode_blkoff;
	Block* blkcell;

	if (!inono)
		return 0;	//file not found.

	inode_blkno = (inono - 1) / 8 + ml_hnd_fsmdl->hnd_fs.start_inode_blkno;

	inode_blkoff = ((inono - 1) % 8) * 128;

	blkcell = tblbm_get_blkcell(inode_blkno);

	if (!blkcell) {
		return 0;
	}
	*pp_dirty_inoblk = &blkcell->dirty;
	return (struct one_inode*) (blkcell->blk + inode_blkoff);

}


/**
 * @brief allocate file from file-table
 */
FileItem* tfl_alloc_filetbl() {
	int i;

	for (i=0; i<TFL_MAX_FILETBL; i++) {
		//if (0 == ml_filetbl[i].inode)
		if (0 == ml_filetbl[i].inode)
			return &ml_filetbl[i];
	}


	return 0;
}


// TODO
// i-nodeのわりあて
// /**
//  * @brief allocate inode from free-list
//  *
//  * @return alocated inode
//  */
// inode* trind_alc_free_inode() {
// 	inode* inode;
//
// 	if (trind_empty_free_inode()) return 0;
//
// 	io_cli();
//
// 	inode = ml_free_inode_head->next_free;
//
// 	trind_remove_from_free(inode);
//
// 	io_sti();
//
// 	return inode;
// }


/**
 * @brief assign file to unused user-file-table
 * @param user_fdtbl user-fd(file descriptor)-table
 * @param hnd_file
 * return file-descriptor (index of user_fdtbl), -1 if error
 */
int tfl_alloc_file_usertbl(FileItem** user_fdtbl, FileItem* hnd_file) {
	int i;

	for (i = 0; i < TFL_MAX_USERFD; i++) {
		if (!user_fdtbl[i]) {
			user_fdtbl[i] = hnd_file;
			return i;
		}
	}
	return -1;
}


// TODO
// 新しいブロックの割当
// /**
//  * @brief allocate blockcell from free-list
//  *
//  * @return alocated blockcell
//  */
// Block* tblbf_alc_free_blkcell() {
// 	Block* blkcell;
//
// 	io_cli();
//
// 	blkcell = ml_free_blk_head->next_free;
//
// 	tblbf_remove_from_free(blkcell);
//
// 	io_sti();
//
// 	return blkcell;
// }


void tfl_init_userfd(FileItem** user_fdtbl) {
	int i;

	for (i=0; i<TFL_MAX_USERFD; i++) {
		user_fdtbl[i] = 0;
	}

}


/**
 * @brief return the index of first param2 character in param1
 * @param s string to be searched
 * @param c search character
 * @return index. error if -1
 */
int stridx(char* s, char c) {
	char* cur;
	int i=0;

	cur = s;

	while(0 != *cur) {
		if (*cur == c) {
			return i;
		}
		cur++; i++;
	}
	return -1;
}


int test_create_syscall(struct SHEET *_sht_back) {
	FileItem *fdtbl[16];
	int fd;
	sht_back = _sht_back;

	tfl_init_userfd(fdtbl);
	fd = te2fo_create("test", fdtbl);

	return fd;
}
