# OS
disk controller
21日目から

# Read/Write
まだテスト段階なので、関数は適当。  
必ず1セクタごとに読み書きするので、与えるアドレスは512byte確保できてる状態で。  
hariboteではintサイズ4byte,shortサイズ2byte
* ide_ata_read_sector_pio(device番号:0,LBA,読み込むセクタの数,先頭アドレス)
* ide_ata_write_sector_pio(device番号:0,LBA,書き込むセクタの数,先頭アドレス)
* ide_ata_write_multiple_sector_pio(device番号:0,LBA,書き込むセクタの数,先頭アドレス)
MULTIPLE MODE使って、2セクタ書き込み後INT発生
READも実装したい。

# 動作確認
* writeに関しては、vdiファイルをhexdumpで確認できる。
* readとかのテストはbootpack.cに記述してる。
