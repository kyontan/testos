/* bootpack�̃��C�� */

#include "bootpack.h"
#include <stdio.h>

#define KEYCMD_LED    0xed

void print_test_title     (struct SHEET *sht_back, int print_x, int *print_y, const char *abs_path, char is_file);
int debug_print_inode_list(struct SHEET *sht_back, int print_x, int *print_y);
int debug_print_blocks    (struct SHEET *sht_back, int print_x, int *print_y, const char *abs_path);
int test_syscall_create   (struct SHEET *sht_back, int print_x, int  print_y, const char *abs_path, char is_file, unsigned int *result_ino);
int test_syscall_read     (struct SHEET *sht_back, int print_x, int  print_y, const char *abs_path, unsigned int size, unsigned int offset, FileEntry *result_file_entry);
int test_syscall_write    (struct SHEET *sht_back, int print_x, int  print_y, const char *abs_path, unsigned char *data, unsigned int size, char is_overwrite, FileEntry *result_file_entry);

void HariMain(void)
{
  struct BOOTINFO *binfo = (struct BOOTINFO *) ADR_BOOTINFO;
  struct SHTCTL *shtctl;
  char s[80];
  struct FIFO32 fifo, keycmd;
  int fifobuf[128], keycmd_buf[32];
  int mx, my, i, cursor_x, cursor_c;
  unsigned int memtotal;
  struct MOUSE_DEC mdec;
  struct MEMMAN *memman = (struct MEMMAN *) MEMMAN_ADDR;
  unsigned char *buf_back, buf_mouse[256], *buf_win, *buf_cons;
  struct SHEET *sht_back, *sht_mouse, *sht_win, *sht_cons;
  struct TASK *task_a, *task_cons;
  struct TIMER *timer;
  int hdd_test,flg,flg2;
  int buf[384];
  buf[0] = 2015;
  buf[1] = 12;
  buf[2] = 5;


  //
  static char keytable0[0x80] = {
    0,   0,   '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '^', 0,   0,
    'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '@', '[', 0,   0,   'A', 'S',
    'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', ':', 0,   0,   ']', 'Z', 'X', 'C', 'V',
    'B', 'N', 'M', ',', '.', '/', 0,   '*', 0,   ' ', 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   '7', '8', '9', '-', '4', '5', '6', '+', '1',
    '2', '3', '0', '.', 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0x5c, 0,  0,   0,   0,   0,   0,   0,   0,   0,   0x5c, 0,  0
  };
  static char keytable1[0x80] = {
    0,   0,   '!', 0x22, '#', '$', '%', '&', 0x27, '(', ')', '~', '=', '~', 0,   0,
    'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '`', '{', 0,   0,   'A', 'S',
    'D', 'F', 'G', 'H', 'J', 'K', 'L', '+', '*', 0,   0,   '}', 'Z', 'X', 'C', 'V',
    'B', 'N', 'M', '<', '>', '?', 0,   '*', 0,   ' ', 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   '7', '8', '9', '-', '4', '5', '6', '+', '1',
    '2', '3', '0', '.', 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   '_', 0,   0,   0,   0,   0,   0,   0,   0,   0,   '|', 0,   0
  };
  int key_to = 0, key_shift = 0, key_leds = (binfo->leds >> 4) & 7, keycmd_wait = -1;

  init_gdtidt();
  init_pic();
  io_sti(); /* IDT/PIC�̏��������I�������̂�CPU�̊��荞�݋֎~������ */
  fifo32_init(&fifo, 128, fifobuf, 0);
  init_pit();
  init_keyboard(&fifo, 256);
  init_waiter();
  // hdd_test = ide_initialize_common_check();
  // flg = ide_initialize_transfer_mode(0);
  ide_initialize_device(0,2);
  enable_mouse(&fifo, 512, &mdec);
  io_out8(PIC0_IMR, 0xf8); /* PIT��PIC1�ƃL�[�{�[�h������(11111000) */
  // io_out8(PIC1_IMR, 0xef); /* �}�E�X������(11101111) */
  io_out8(PIC1_IMR, 0xaf); /* �}�E�X��HDD������(10101111) */
  fifo32_init(&keycmd, 32, keycmd_buf, 0);

  memtotal = memtest(0x00400000, 0xbfffffff);
  memman_init(memman);
  memman_free(memman, 0x00001000, 0x0009e000); /* 0x00001000 - 0x0009efff */
  memman_free(memman, 0x00400000, memtotal - 0x00400000);

  init_palette();
  shtctl = shtctl_init(memman, binfo->vram, binfo->scrnx, binfo->scrny);
  task_a = task_init(memman);
  fifo.task = task_a;
  task_run(task_a, 1, 2);

  /* sht_back */
  sht_back  = sheet_alloc(shtctl);
  buf_back  = (unsigned char *) memman_alloc_4k(memman, binfo->scrnx * binfo->scrny);
  sheet_setbuf(sht_back, buf_back, binfo->scrnx, binfo->scrny, -1); /* �����F�Ȃ� */
  init_screen8(buf_back, binfo->scrnx, binfo->scrny);

  // /* sht_cons */
  // sht_cons = sheet_alloc(shtctl);
  // buf_cons = (unsigned char *) memman_alloc_4k(memman, 256 * 165);
  // sheet_setbuf(sht_cons, buf_cons, 256, 165, -1); /* �����F�Ȃ� */
  // make_window8(buf_cons, 256, 165, "console", 0);
  // make_textbox8(sht_cons, 8, 28, 240, 128, COL8_000000);
  // task_cons = task_alloc();
  // task_cons->tss.esp = memman_alloc_4k(memman, 64 * 1024) + 64 * 1024 - 12;
  // task_cons->tss.eip = (int) &console_task;
  // task_cons->tss.es = 1 * 8;
  // task_cons->tss.cs = 2 * 8;
  // task_cons->tss.ss = 1 * 8;
  // task_cons->tss.ds = 1 * 8;
  // task_cons->tss.fs = 1 * 8;
  // task_cons->tss.gs = 1 * 8;
  // *((int *) (task_cons->tss.esp + 4)) = (int) sht_cons;
  // *((int *) (task_cons->tss.esp + 8)) = memtotal;
  // task_run(task_cons, 2, 2); /* level=2, priority=2 */

  // /* sht_win */
  // sht_win   = sheet_alloc(shtctl);
  // buf_win   = (unsigned char *) memman_alloc_4k(memman, 160 * 52);
  // sheet_setbuf(sht_win, buf_win, 144, 52, -1); /* �����F�Ȃ� */
  // make_window8(buf_win, 144, 52, "task_a", 1);
  // make_textbox8(sht_win, 8, 28, 128, 16, COL8_FFFFFF);
  // cursor_x = 8;
  // cursor_c = COL8_FFFFFF;
  // timer = timer_alloc();
  // timer_init(timer, &fifo, 1);
  // timer_settime(timer, 50);

  /* sht_mouse */
  sht_mouse = sheet_alloc(shtctl);
  sheet_setbuf(sht_mouse, buf_mouse, 16, 16, 99);
  init_mouse_cursor8(buf_mouse, 99);
  mx = (binfo->scrnx - 16) / 2; /* ���ʒ����ɂȂ��悤�ɍ��W�v�Z */
  my = (binfo->scrny - 28 - 16) / 2;

  sheet_slide(sht_back,  0,  0);
  sheet_slide(sht_cons, 32,  4);
  sheet_slide(sht_win,  64, 56);
  sheet_slide(sht_mouse, mx, my);
  sheet_updown(sht_back,  0);
  sheet_updown(sht_cons,  1);
  sheet_updown(sht_win,   2);
  sheet_updown(sht_mouse, 3);

  /* �ŏ��ɃL�[�{�[�h���ԂƂ̐H���Ⴂ���Ȃ��悤�ɁA�ݒ肵�Ă������Ƃɂ��� */
  fifo32_put(&keycmd, KEYCMD_LED);
  fifo32_put(&keycmd, key_leds);

  int print_y = 0;

  // sprintf(s, "%x",hdd_test);
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, (hdd_test==DEV_ATA)?"It's ATA":s, 40);
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, "---------SIZE-------", 100);
  // sprintf(s, "int: %d",sizeof(int));
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // sprintf(s, "short int: %d",sizeof(short));
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // sprintf(s, "long int: %d",sizeof(long int));
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // sprintf(s, "char: %d",sizeof(char));
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, "------HDD READ & WRITE---------", 100);
  // sprintf(s, "INITIALIZE TRSF MODE(): %d",flg);
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // sprintf(s, "PIO_MODE: %d",ide_get_pio_mode(0));
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 40);
  // flg = ide_ata_write_multiple_sector_pio(0,20,3,buf);
  // flg2 = ide_ata_read_multiple_sector_pio(0,20,3,buf);
  // flg = ide_ata_write_sector_pio(0,18,1,buf);
  // flg2 = ide_ata_read_sector_pio(0,18,1,rbuf);
  // flg = ide_ata_write_sector_pio(0,19,1,buf);
  // flg2 = ide_ata_read_sector_pio(0,19,1,rbuf);
  // sprintf(s, "WRTIE:%d READ:%d READ DATA: %d, %d, %d",flg,flg2,buf[0],buf[1],buf[2]);
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  // sprintf(s, "INTERRUPTS: %d",ide_get_is_interrupt());
  // putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, "------FILE SYSTEM---------", 100);
  int result, flg_write = 0;
  unsigned int result_ino;
  unsigned char result_data[16];
  unsigned char file_path[64];
  FileEntry *result_file_entry;

  unsigned int print_x = 0;

  result = make_superfs(0, 128);
  sprintf(s, "make_superfs(0, 128) #=> %d", result);
  putfonts8_asc_sht(sht_back, 0, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

  // /test file
  if (0) {
    int is_file = 1;
    strcpy(file_path, "/test");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    unsigned char data_to_write[16] = "ABCDEFG";
    result = test_syscall_write(sht_back, print_x, (print_y += 20), file_path, data_to_write, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    result = test_syscall_read(sht_back, print_x, (print_y += 20), file_path, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    sprintf(s, "test_find_block #=> %s", test_find_block(file_path));
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    if (result != 0) { goto after_test; }
  }

  // /hoge file
  if (0) {
    int is_file = 1;
    strcpy(file_path, "/hoge");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    unsigned char data_to_write[16] = "1234567890";
    result = test_syscall_write(sht_back, print_x, (print_y += 20), file_path, data_to_write, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    result = test_syscall_read(sht_back, print_x, (print_y += 20), file_path, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    sprintf(s, "test_find_block #=> %s", test_find_block(file_path));
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    if (result != 0) { goto after_test; }
  }

  // /mashiro directory
  if (1) {
    int is_file = 0;
    strcpy(file_path, "/mashiro");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    // result = test_find_inode(file_path);
    // sprintf(s, "test_find_inode(\"%s\") #=> %d", file_path, result);
    // putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  }

  if (1) {
    int is_file = 0;
    strcpy(file_path, "/mashiro/dinner");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    sprintf(s, "file_path: %d, %s", file_path, file_path);
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    sprintf(s, "file_path: %d, %s", file_path, file_path);
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

    // result = test_find_inode(file_path);
    // sprintf(s, "test_find_inode(\"%s\") #=> %d", file_path, result);
    // putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  }

  if (1) {
    int is_file = 1;
    strcpy(file_path, "/mashiro/dinner/curry");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    unsigned char data_to_write[16] = "amakuchi";
    result = test_syscall_write(sht_back, print_x, (print_y += 20), file_path, data_to_write, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    result = test_syscall_read(sht_back, print_x, (print_y += 20), file_path, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    sprintf(s, "test_find_block #=> %s", test_find_block(file_path));
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  }

  print_y += 20;
  debug_print_inode_list(sht_back, print_x, &print_y);
  print_y += 20;
  debug_print_blocks(sht_back, print_x, &print_y, file_path);

  if (1) {
    int is_file = 0;
    strcpy(file_path, "/mashiro/sweets");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }
  }

  if (1) {
    int is_file = 1;
    strcpy(file_path, "/mashiro/sweets/scone");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    unsigned char data_to_write[16] = "clotted cream";
    result = test_syscall_write(sht_back, print_x, (print_y += 20), file_path, data_to_write, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    result = test_syscall_read(sht_back, print_x, (print_y += 20), file_path, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    sprintf(s, "test_find_block #=> %s", test_find_block(file_path));
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  }
  print_y += 20;
  debug_print_blocks(sht_back, print_x, &print_y, file_path);

  if (1) {
    int is_file = 0;
    FSEntry **return_entry;
    int return_size;
    strcpy(file_path, "/mashiro/sweets");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = syscall_get_fsentry(file_path, return_entry, &return_size);
    sprintf(s, "syscall_get_fsentry(\"%s\") #=> %d, entry: %d, size: %d", file_path, result, *return_entry, return_size);
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    if (result != 0) { goto after_test; }

    FileEntry *entry;
    unsigned int i;
    for (i = 0; i < return_size; i++) {
      entry = &(return_entry[i]->file);
      sprintf(s, "addr: %d, ino: %d, parent_ino: %d, f_name: %s", entry, entry->ino, entry->parent_ino, entry->f_name);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    }
  }


  // remove /mashiro/dinner dir (not recursive, expect error)
  if (0) {
    strcpy(file_path, "/mashiro/dinner");

    int is_recursive = 0;
    sprintf(s, "## test: \"%s\" (%s)", file_path, is_recursive ? "recursive" : "not recursive");
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

    result = test_syscall_remove(sht_back, print_x, (print_y += 20), file_path, is_recursive);
    if (result == 0) { goto after_test; } // this test should return "not" 0
  }

  // remove /mashiro/dinner dir (recursive)
  if (1) {
    strcpy(file_path, "/mashiro/dinner");

    int is_recursive = 1;
    sprintf(s, "## test: \"%s\" (%s)", file_path, is_recursive ? "recursive" : "not recursive");
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

    result = test_syscall_remove(sht_back, print_x, (print_y += 20), file_path, is_recursive);
    if (result != 0) { goto after_test; }
  }

  print_y += 20;
  debug_print_inode_list(sht_back, print_x, &print_y);

  print_x = 560; print_y = 0;

  if (1) {
    int is_file = 1;
    strcpy(file_path, "/mashiro/sweets/cake");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }
  }

  print_y += 20;
  debug_print_inode_list(sht_back, print_x, &print_y);



  //  path test
  // /mashiro/./food/../is_uma file
  if (1) {
    int is_file = 1;
    strcpy(file_path, "/mashiro/./sweets/../sweets/scone");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_read(sht_back, print_x, (print_y += 20), file_path, 16, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    sprintf(s, "test_find_block #=> %s", test_find_block(file_path));
    putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
  }

  if (1) {
    int is_file = 1;
    strcpy(file_path, "/big_data");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    unsigned int data_size = BLOCK_DATA_LEN * 17 + 32;
    unsigned char *data_to_write = memman_alloc(memman, data_size);
    if (data_to_write == NULL) { goto after_test; }

    unsigned int i;
    for (i = 0; i < data_size; i++) {
      unsigned char block_count = (i + 1) / BLOCK_DATA_LEN;
      if ((i + 1) % BLOCK_DATA_LEN) {
        block_count++;
      }

      data_to_write[i]  = (block_count % 16) << 4;
      data_to_write[i] |= i % 15;
      // data_to_write[i] = i % 256;
    }

    result = test_syscall_write(sht_back, print_x, (print_y += 20), file_path, data_to_write, data_size, 0, result_file_entry);
    if (result != 0) { goto after_test; }

    memman_free(memman, data_to_write, data_size);
    print_y += 20;
    debug_print_blocks(sht_back, print_x, &print_y, file_path);
  }

  // file descriptor test
  if (1) {
    char buf[32];
    int is_file = 1, len;
    unsigned int mode;
    strcpy(file_path, "/aokana");
    print_test_title(sht_back, print_x, (print_y += 20), file_path, is_file);

    result = test_syscall_create(sht_back, print_x, (print_y += 20), file_path, is_file, &result_ino);
    if (result != 0) { goto after_test; }

    {
      mode = FD_MODE_WRITE;
      FileDescriptor *fd = syscall_open(file_path, mode);
      sprintf(s, "syscall_open(%s, %d) #=> addr: %d", file_path, mode, fd);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      strcpy(buf, "kurashina asuka");
      len = strlen(buf) + 1;
      result = syscall_write_new(fd, buf, len);
      sprintf(s, "syscall_write_new(%d) #=> %d (pos: %d, size: %d)", len, result, fd->pos, fd->file_entry->size);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      strcpy(buf, "arisaka mashiro");
      len = strlen(buf) + 1;
      result = syscall_write_new(fd, buf, len);
      sprintf(s, "syscall_write_new(%d) #=> %d (pos: %d, size: %d)", len, result, fd->pos, fd->file_entry->size);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      result = syscall_close(fd);
      sprintf(s, "syscall_close(fd) #=> %d", result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    }

    {
      char read_result[256];
      mode = FD_MODE_READ_WRITE;
      FileDescriptor *fd = syscall_open(file_path, mode);
      sprintf(s, "syscall_open(%s, %d) #=> addr: %d", file_path, mode, fd);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      int offset = 4090, is_relative = 0;
      result = syscall_seek(fd, offset, is_relative);
      sprintf(s, "syscall_seek(fd, %d, %s) #=> %d", offset, is_relative ? "rel" : "abs", result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      strcpy(buf, "tobisawa misaki");
      len = strlen(buf) + 1;
      result = syscall_write_new(fd, buf, len);
      sprintf(s, "syscall_write_new(%d) #=> %d (pos: %d, size: %d)", len, result, fd->pos, fd->file_entry->size);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      offset = -(strlen(buf) + 1), is_relative = 1;
      result = syscall_seek(fd, offset, is_relative);
      sprintf(s, "syscall_seek(fd, %d, %s) #=> %d", offset, is_relative ? "rel" : "abs", result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      result = syscall_read_new(fd, len, read_result);
      sprintf(s, "syscall_read_new(%d) #=> %d (fd->pos: %d)", len, result, fd->pos);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      result = syscall_close(fd);
      sprintf(s, "syscall_close(fd) #=> %d", result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      sprintf(s, "read_result: %s", read_result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    }

    {
      mode = FD_MODE_WRITE | FD_MODE_APPEND;
      FileDescriptor *fd = syscall_open(file_path, mode);
      sprintf(s, "syscall_open(%s, %d) #=> addr: %d", file_path, mode, fd);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      strcpy(buf, "ichinose rika");
      len = strlen(buf) + 1;
      result = syscall_write_new(fd, buf, len);
      sprintf(s, "syscall_write_new(%d) #=> %d (pos: %d, size: %d)", len, result, fd->pos, fd->file_entry->size);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

      result = syscall_close(fd);
      sprintf(s, "syscall_close(fd) #=> %d", result);
      putfonts8_asc_sht(sht_back, print_x, (print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);
    }
  }

after_test:

  // Block* testblock = get_block_by_blockno(0);
  // struct one_inode* p_inode = (struct one_inode*)testblock->block_data;
  // p_inode++;
  // super_mdl->root_inode = p_inode;
  // super_mdl->handler.start_inode_blkno = testblock->block_no;
  //
  // sprintf(s, "start_inode_blockno : %d", testblock->block_no);
  // putfonts8_asc_sht(sht_back, 0, 570, COL8_FFFFFF, COL8_008484, s, 200);
  //
  // sprintf(s, "SYSTEM CALL 'CREATE' : /test");
  // putfonts8_asc_sht(sht_back, 0, 590, COL8_FFFFFF, COL8_008484, s, 200);
  // for (i=0; i<16; i++) {
  //  fdtbl[i] = 0;
  // }
  // fd = syscall_create("test", fdtbl);
  // sprintf(s, "RESULT : %d", fd);
  // putfonts8_asc_sht(sht_back, 0, 610, COL8_FFFFFF, COL8_008484, s, 200);


  for (;;) {
    if (fifo32_status(&keycmd) > 0 && keycmd_wait < 0) {
      /* �L�[�{�[�h�R���g���[���ɑ����f�[�^�������΁A���� */
      keycmd_wait = fifo32_get(&keycmd);
      wait_KBC_sendready();
      io_out8(PORT_KEYDAT, keycmd_wait);
    }
    io_cli();
    if (fifo32_status(&fifo) == 0) {
      task_sleep(task_a);
      io_sti();
    } else {
      i = fifo32_get(&fifo);
      io_sti();
      if (256 <= i && i <= 511) { /* �L�[�{�[�h�f�[�^ */
      //   if (i < 0x80 + 256) { /* �L�[�R�[�h�𕶎��R�[�h�ɕϊ� */
      //     if (key_shift == 0) {
      //       s[0] = keytable0[i - 256];
      //     } else {
      //       s[0] = keytable1[i - 256];
      //     }
      //   } else {
      //     s[0] = 0;
      //   }
      //   if ('A' <= s[0] && s[0] <= 'Z') { /* ���͕������A���t�@�x�b�g */
      //     if (((key_leds & 4) == 0 && key_shift == 0) ||
      //         ((key_leds & 4) != 0 && key_shift != 0)) {
      //       s[0] += 0x20; /* �啶�����������ɕϊ� */
      //     }
      //   }
      //   if (s[0] != 0) { /* �ʏ핶�� */
      //     if (key_to == 0) {  /* �^�X�NA�� */
      //       if (cursor_x < 128) {
      //         /* �ꕶ���\�����Ă����A�J�[�\����1�i�߂� */
      //         s[1] = 0;
      //         putfonts8_asc_sht(sht_win, cursor_x, 28, COL8_000000, COL8_FFFFFF, s, 1);
      //         cursor_x += 8;
      //       }
      //     } else {  /* �R���\�[���� */
      //       fifo32_put(&task_cons->fifo, s[0] + 256);
      //     }
      //   }
      //   if (i == 256 + 0x0e) {  /* �o�b�N�X�y�[�X */
      //     if (key_to == 0) {  /* �^�X�NA�� */
      //       if (cursor_x > 8) {
      //         /* �J�[�\�����X�y�[�X�ŏ����Ă����A�J�[�\����1�߂� */
      //         putfonts8_asc_sht(sht_win, cursor_x, 28, COL8_000000, COL8_FFFFFF, " ", 1);
      //         cursor_x -= 8;
      //       }
      //     } else {  /* �R���\�[���� */
      //       fifo32_put(&task_cons->fifo, 8 + 256);
      //     }
      //   }
      //   if (i == 256 + 0x1c) {  /* Enter */
      //     if (key_to != 0) {  /* �R���\�[���� */
      //       fifo32_put(&task_cons->fifo, 10 + 256);
      //     }
      //   }
      //   if (i == 256 + 0x0f) {  /* Tab */
      //     if (key_to == 0) {
      //       key_to = 1;
      //       make_wtitle8(buf_win,  sht_win->bxsize,  "task_a",  0);
      //       make_wtitle8(buf_cons, sht_cons->bxsize, "console", 1);
      //       cursor_c = -1; /* �J�[�\�������� */
      //       boxfill8(sht_win->buf, sht_win->bxsize, COL8_FFFFFF, cursor_x, 28, cursor_x + 7, 43);
      //       fifo32_put(&task_cons->fifo, 2); /* �R���\�[���̃J�[�\��ON */
      //     } else {
      //       key_to = 0;
      //       make_wtitle8(buf_win,  sht_win->bxsize,  "task_a",  1);
      //       make_wtitle8(buf_cons, sht_cons->bxsize, "console", 0);
      //       cursor_c = COL8_000000; /* �J�[�\�����o�� */
      //       fifo32_put(&task_cons->fifo, 3); /* �R���\�[���̃J�[�\��OFF */
      //     }
      //     sheet_refresh(sht_win,  0, 0, sht_win->bxsize,  21);
      //     sheet_refresh(sht_cons, 0, 0, sht_cons->bxsize, 21);
      //   }
      //   if (i == 256 + 0x2a) {  /* ���V�t�g ON */
      //     key_shift |= 1;
      //   }
      //   if (i == 256 + 0x36) {  /* �E�V�t�g ON */
      //     key_shift |= 2;
      //   }
      //   if (i == 256 + 0xaa) {  /* ���V�t�g OFF */
      //     key_shift &= ~1;
      //   }
      //   if (i == 256 + 0xb6) {  /* �E�V�t�g OFF */
      //     key_shift &= ~2;
      //   }
      //   if (i == 256 + 0x3a) {  /* CapsLock */
      //     key_leds ^= 4;
      //     fifo32_put(&keycmd, KEYCMD_LED);
      //     fifo32_put(&keycmd, key_leds);
      //   }
      //   if (i == 256 + 0x45) {  /* NumLock */
      //     key_leds ^= 2;
      //     fifo32_put(&keycmd, KEYCMD_LED);
      //     fifo32_put(&keycmd, key_leds);
      //   }
      //   if (i == 256 + 0x46) {  /* ScrollLock */
      //     key_leds ^= 1;
      //     fifo32_put(&keycmd, KEYCMD_LED);
      //     fifo32_put(&keycmd, key_leds);
      //   }
      //   if (i == 256 + 0xfa) {  /* �L�[�{�[�h���f�[�^�𖳎��Ɏ󂯎����� */
      //     keycmd_wait = -1;
      //   }
      //   if (i == 256 + 0xfe) {  /* �L�[�{�[�h���f�[�^�𖳎��Ɏ󂯎����Ȃ����� */
      //     wait_KBC_sendready();
      //     io_out8(PORT_KEYDAT, keycmd_wait);
      //   }
        /* �J�[�\���̍ĕ\�� */
        if (cursor_c >= 0) {
          boxfill8(sht_win->buf, sht_win->bxsize, cursor_c, cursor_x, 28, cursor_x + 7, 43);
        }
        sheet_refresh(sht_win, cursor_x, 28, cursor_x + 8, 44);
      } else if (512 <= i && i <= 767) { /* �}�E�X�f�[�^ */
        if (mouse_decode(&mdec, i - 512) != 0) {
          /* �}�E�X�J�[�\���̈ړ� */
          mx += mdec.x;
          my += mdec.y;
          if (mx < 0) {
            mx = 0;
          }
          if (my < 0) {
            my = 0;
          }
          if (mx > binfo->scrnx - 1) {
            mx = binfo->scrnx - 1;
          }
          if (my > binfo->scrny - 1) {
            my = binfo->scrny - 1;
          }
          sheet_slide(sht_mouse, mx, my);
          if ((mdec.btn & 0x01) != 0) {
            /* ���{�^���������Ă������Asht_win�𓮂��� */
            sheet_slide(sht_win, mx - 80, my - 8);
          }
        }
      } else if (i <= 1) { /* �J�[�\���p�^�C�} */
        if (i != 0) {
          timer_init(timer, &fifo, 0); /* ����0�� */
          if (cursor_c >= 0) {
            cursor_c = COL8_000000;
          }
        } else {
          timer_init(timer, &fifo, 1); /* ����1�� */
          if (cursor_c >= 0) {
            cursor_c = COL8_FFFFFF;
          }
        }
        timer_settime(timer, 50);
        if (cursor_c >= 0) {
          boxfill8(sht_win->buf, sht_win->bxsize, cursor_c, cursor_x, 28, cursor_x + 7, 43);
          sheet_refresh(sht_win, cursor_x, 28, cursor_x + 8, 44);
        }
      }
    }
  }
}


void print_test_title(struct SHEET *sht_back, int print_x, int *print_y, const char *abs_path, char is_file) {
  char s[80];

  sprintf(s, "## test: \"%s\" (%s)", abs_path, is_file ? "file" : "directory");
  putfonts8_asc_sht(sht_back, print_x, print_y, COL8_FFFFFF, COL8_008484, s, 200);
}

int debug_print_inode_list(struct SHEET *sht_back, int print_x, int *print_y) {
  char s[40];

  struct one_inode *inode = test_get_root_inode();
  *print_y -= 20;

  do {
    sprintf(s, "ino: %d, name: %s", inode->ino, inode->entry->common.name);
    putfonts8_asc_sht(sht_back, print_x, (*print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

  } while((inode = inode->next));

  return 0;
}

int debug_print_blocks(struct SHEET *sht_back, int print_x, int *print_y, const char *abs_path) {
  char s[40];

  *print_y -= 20;
  struct one_inode *inode = get_inode_by_path(abs_path);
  if (inode == NULL || (inode->mode & ONE_INODE_TYPE_DIR)) { return 1; }

  unsigned int block_idx = 0;
  BlockGroup *block_group = inode->entry->file.block_group;

  while (1) {
    if (block_idx == BLOCK_GROUP_BLOCKS_NUM) {
      block_group = block_group->next;

      if (block_group == NULL) {
        break;
      }
    }

    Block *block = block_group->blocks[block_idx % BLOCK_GROUP_BLOCKS_NUM];
    if (block == NULL) {
      break;
    }

    sprintf(s, "%d: block_no: %d, addr: %d", block_idx, block->block_no, block);
    putfonts8_asc_sht(sht_back, print_x, (*print_y += 20), COL8_FFFFFF, COL8_008484, s, 200);

    block_idx++;
  };

  return 0;
}

int test_syscall_create(struct SHEET *sht_back, int print_x, int print_y,
                        const char *abs_path, char is_file, unsigned int *result_ino) {
  char s[80];
  int result = syscall_create(abs_path, is_file, result_ino);
  sprintf(s, "syscall_create(\"%s\", %d) #=> %d, ino: %d", abs_path, is_file, result, *result_ino);
  putfonts8_asc_sht(sht_back, print_x, print_y, COL8_FFFFFF, COL8_008484, s, 200);

  return result;
}

int test_syscall_write(struct SHEET *sht_back, int print_x, int print_y,
                       const char *abs_path, unsigned char *data, unsigned int size, char is_overwrite, FileEntry *result_file_entry) {
  char s[80];
  int result = syscall_write(abs_path, data, size, is_overwrite, result_file_entry);
  sprintf(s, "syscall_write #=> %d", result);
  putfonts8_asc_sht(sht_back, print_x, print_y, COL8_FFFFFF, COL8_008484, s, 200);

  return result;
}

int test_syscall_read(struct SHEET *sht_back, int print_x, int print_y,
                       const char *abs_path, unsigned int size, unsigned int offset, FileEntry *result_file_entry) {
  char s[80];
  int result = syscall_read(abs_path, size, offset, result_file_entry);
  sprintf(s, "syscall_read #=> %d", result);
  putfonts8_asc_sht(sht_back, print_x, print_y, COL8_FFFFFF, COL8_008484, s, 200);

  return result;
}



int test_syscall_remove(struct SHEET *sht_back, int print_x, int print_y,
                        const char *abs_path, char is_recursive) {
  char s[80];
  int result = syscall_remove(abs_path, is_recursive);
  sprintf(s, "syscall_remove(\"%s\", %d) #=> %d", abs_path, is_recursive, result);
  putfonts8_asc_sht(sht_back, print_x, print_y, COL8_FFFFFF, COL8_008484, s, 200);

  return result;
}
